# Changes

## 2.0.0 (2024-12-19)

- This is the first release after forking from ligo-segments. It should be a
  drop-in replacement: just replace `from ligo import segments` with
  `import igwn_segemnts as segments`.
- Drop support for Python 2.
- Fix build on Python 3.12 and 3.13.
- Add builds for Windows.
